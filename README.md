### Installation ###
```
composer create-project wordrobe/skeleton your-project-path
```

### Security ###
To grant an higher security level, you can rename the default string "wp" for both database tables prefix and Wordpress installation folder. You can find all references in the following files:
- composer.json (["extra"]["wordpress-install-dir"])
- .env(.dist|.staging|.production) (DB_PREFIX, WP_FOLDER)
- .gitignore (under "WORDPRESS")
- wp-cli.yml (path)
- web (wp folder name)
- web/.htaccess (under "Bedrock Ajax Redirect")
- web/index.php