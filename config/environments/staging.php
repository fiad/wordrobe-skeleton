<?php
/**
 * Configuration overrides for WP_ENV === 'staging'
 */

use Roots\WPConfig\Config;

Config::define('WP_DEBUG', true);
Config::define('WP_DEBUG_DISPLAY', true);
Config::define('SCRIPT_DEBUG', true);

ini_set('display_errors', 1);

// Disable plugin and theme updates and installation from the admin
Config::define('DISALLOW_FILE_MODS', true);